import asyncio
import pathlib
import sys


UI_DIRECTORY = pathlib.Path(__file__).parent
UI_FILES_DIRECTORY = UI_DIRECTORY.joinpath('ui_files')
VIEWS_DIRECTORY = UI_DIRECTORY.joinpath('views')


async def prepare_transform_ui_file(ui_path, static_path):
    cmd = f'pyuic5 -x "{ui_path}" -o "{static_path}"'
    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)

    stdout, stderr = await proc.communicate()

    print(f"[{cmd!r} exited with {proc.returncode}]")
    if stdout:
        print(f'[stdout]\n{stdout.decode()}')
    if stderr:
        print(f'[stderr]\n{stderr.decode()}')


def transform_ui_directory(ui_dir=UI_FILES_DIRECTORY, view_dir=VIEWS_DIRECTORY):
    # For the Windows platform it is necessary to use WindowsProactorEventLoopPolicy
        # see the doc about selectors in different OS's
    if sys.platform == 'win32':
        asyncio.set_event_loop_policy(
            asyncio.WindowsProactorEventLoopPolicy())
    # Build a list of input for the command to execute.
    input_list = []
    for ui_file in ui_dir.iterdir():
        if ui_file.suffix == '.ui':
            py_file_name = ui_file.with_suffix('.py')
            view_name = f"{py_file_name.name}"
            view_path = view_dir.joinpath(view_name)
            input_list.append((str(ui_file), str(view_path)))
    print("Runing the async process.")
    processors = [prepare_transform_ui_file(x, y) for (x, y) in input_list]

    async def main():
        await asyncio.gather(*processors)

    asyncio.run(main())


if __name__ == '__main__':
    transform_ui_directory()
