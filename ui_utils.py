import sys
from PyQt5 import QtWidgets


FORMAT_MAPPER = {
    'all': "All Files (*)",
    'csv': "CSV (*.csv);; All Files (*)",
    'excel': "Excel 2007 (*.xlsx);; All Files (*)",
    'xml': "XML (*.xml);; All Files (*)",
}


def file_dialog(directory=False, save=False, name_suggestion='', format_principal='all'):
    # Si le choix porte sur un dossier
    if directory:
        fd = dialog_directory()
        return fd
    elif save:
        fd = dialog_save_file(name_suggestion, format_principal)
        return fd
    else:
        fd = dialog_select_file(format_principal)
        return fd


def dialog_directory():
    fd = QtWidgets.QFileDialog()
    selection_mode = QtWidgets.QFileDialog.Directory
    accept_mode = QtWidgets.QFileDialog.AcceptOpen
    fd.setFileMode(selection_mode)
    fd.setAcceptMode(accept_mode)
    return fd


def dialog_save_file(name_suggestion='', format_principal='all'):
    fd = QtWidgets.QFileDialog()
    accept_mode = QtWidgets.QFileDialog.AcceptSave
    selection_mode = QtWidgets.QFileDialog.AnyFile
    fd.setFileMode(selection_mode)
    fd.setAcceptMode(accept_mode)
    fd.setNameFilter(FORMAT_MAPPER.get(format_principal, format_principal))
    if name_suggestion:
        fd.selectFile(name_suggestion)
    return fd


def dialog_select_file(format_principal='all'):
    fd = QtWidgets.QFileDialog()
    accept_mode = QtWidgets.QFileDialog.AcceptOpen
    selection_mode = QtWidgets.QFileDialog.ExistingFile
    fd.setFileMode(selection_mode)
    fd.setAcceptMode(accept_mode)
    fd.setNameFilter(FORMAT_MAPPER.get(format_principal, format_principal))
    return fd


def fire_alert(msg, *, info=None, details=None):
    box = QtWidgets.QMessageBox()
    box.setText(msg)
    if info:
        box.setInformativeText(info)
    if details:
        box.setDetailedText(details)
    box.setIcon(QtWidgets.QMessageBox.Warning)
    box.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
    box.setDefaultButton(QtWidgets.QMessageBox.Ok)
    ret = box.exec_()
    return ret


def fire_maybe_continue(msg, *, info=None, details=None):
    box = QtWidgets.QMessageBox()
    box.setText(msg)
    if info:
        box.setInformativeText(info)
    if details:
        box.setDetailedText(details)
    box.setIcon(QtWidgets.QMessageBox.Question)
    box.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
    ret = box.exec_()
    return ret


def fire_info(msg, *, info=None, details=None):
    box = QtWidgets.QMessageBox()
    box.setText(msg)
    if info:
        box.setInformativeText(info)
    if details:
        box.setDetailedText(details)
    box.setIcon(QtWidgets.QMessageBox.Information)
    box.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
    ret = box.exec_()
    return ret


if __name__ == '__main__':
    try:
        app = QtWidgets.QApplication(sys.argv)
        msg = 'Message Principal'
        info = 'Explication light du message'
        detail = "Pour plus de détails: afin d'aider l'utilisateur à mieux comprendre le message"
        fd = file_dialog(save=True)
        fd.exec_()
        app.exec_(sys.exit(0))
    except NameError:
        print(''.join(["Name Error:", sys.exc_info()[1]]))
    except SystemExit:
        print("Closing Window...")
    except Exception:
        print(sys.exc_info()[1])
