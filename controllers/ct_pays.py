from PyQt5 import QtWidgets

from hexagon import db

from hxui.views import Ui_Pays
from hxui.ui_utils import fire_alert, fire_info


class CT_Pays(QtWidgets.QWidget):

    def __init__(self, adapter=None):
        super(CT_Pays, self).__init__()
        self.ui = Ui_Pays()
        self.ui.setupUi(self)
        self._setup_defaults()
        self._connect_actions()

    def _setup_defaults(self):
        # Set the completion model
        mar = self._load_from_db()
        self.ui.txt_id.setText(str(mar.id))
        self.ui.txt_description.setText(str(mar.description))
        self.ui.txt_alpha2.setText(str(mar.alpha2))

    def _connect_actions(self):
        # CurrentIndex pour les dates afin de prendre en considération une date entière
        # Lors de la conversion d'une date string vers une date Python
        self.ui.bu_add.clicked.connect(self.add_instance)
        self.ui.bu_delete.clicked.connect(self.delete_instance)
        self.ui.bu_close.clicked.connect(self.close_instance)

    def add_instance(self):
        msg = f"instance ajoutée"
        fire_info(msg)

    def delete_instance(self):
        msg = f"instance non supprimée"
        fire_alert(msg, details='Voir les détails')

    def close_instance(self):
        pass

    def _load_from_db(self, code_pays='MAR'):
        pays = db.Pays.by_alpha3(code_pays)
        return pays


if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = QtWidgets.QMainWindow()
    window.setWindowTitle('****** ECRAN PAYS ******')
    wdgt = CT_Pays()
    window.setCentralWidget(wdgt)
    window.show()
    sys.exit(app.exec_())
